package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        try{
            int i, fac = 1;
            System.out.println("Input an integer whose factorial will be computed");
            int num = scanner.nextInt();
            if(num > -1){
                for (i = 1; i <= num; i++){
                    fac = fac*i;
                }
            }else{
                throw new Exception();
            }
            System.out.println("The factorial of " + num + " is " + fac);
        }catch(Exception e){
            System.out.println("Input Invalid");
        }
    }

}
